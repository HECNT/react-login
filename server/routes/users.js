import express from 'express';
import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

let router = express.Router();

function validateInput(data) {
  let errors = {};

  if (Validator.isNull(data.username)) {
    errors.username = 'Este campo es requerido'
  }

  if (Validator.isNull(data.email)) {
    errors.email = 'Este campo es requerido'
  }

  if (!Validator.isEmail(data.email)) {
    errors.email = 'Correo electronico es invalido'
  }

  if (Validator.isNull(data.password)) {
    errors.password = 'Este campo es requerido'
  }

  if (Validator.isNull(data.passwordConfirmation)) {
    errors.passwordConfirmation = 'Este campo es requerido'
  }
  if (!Validator.equals(data.password, data.passwordConfirmation)) {
    errors.passwordConfirmation = 'Los password no hacen match'
  }
  if (Validator.isNull(data.timezone)) {
    errors.timezone = 'Este campo es requerido'
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}

router.post('/', (req, res) => {
  const { errors, isValid } = validateInput(req.body);

  setTimeout(function () {
    if (!isValid) {
      res.status(400).json(errors);
    }
  }, 5000);
})

export default router;
